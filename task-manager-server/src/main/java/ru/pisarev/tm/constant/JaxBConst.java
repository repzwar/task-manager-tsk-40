package ru.pisarev.tm.constant;

public interface JaxBConst {

    String FACTORY = "javax.xml.bind.context.factory";

    String JAXBFACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    String TYPE = "eclipselink.media-type";

    String JSON = "application/json";

}
